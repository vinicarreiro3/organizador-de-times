
import { useState } from 'react';
import './App.css';
import Display from './components/Display/Display';
import Adicionar from './components/Adicionar/Adicionar';


function App() {

  const [pessoas,setPessoas] = useState([])

  return (
    <div className="App">
      <header className="App-header">

        <h1 className="titulo">Organizador de time</h1>
        <p>Faz até 4 times (azul, vermelho, verde, amarelo),
          para funcionar tem que escrever o nome do usuario e a cor do time dele tudo em minusculo</p>
        <Adicionar setPessoas={setPessoas} Pessoas={pessoas}/>
        <Display pessoas={pessoas}/>
      </header>
    </div>
  );
}

export default App;
