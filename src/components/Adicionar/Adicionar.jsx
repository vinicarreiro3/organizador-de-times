import { useState } from "react"


const Adicionar =({Pessoas,setPessoas}) =>{

        const [nome,setNome] = useState();
        const [team,setTeam] = useState();

        const options = [
            { value: 'chocolate', label: 'Chocolate' },
            { value: 'strawberry', label: 'Strawberry' },
            { value: 'vanilla', label: 'Vanilla' }
          ]
    const handleAdd = () =>{
        setPessoas([...Pessoas,{name:nome,time:team}])
        }
       return(<>
       <input value={nome} onChange={(event) => setNome(event.target.value)} placeholder='Nome (exemplo: Carlos)'/>
       <select value={team} onChange={(event) => setTeam(event.target.value)} placeholder='Time (exemplo: Amarelo)'>
         <option value="azul">Azul</option>
         <option value="vermelho">Vermelho</option>
         <option value="verde">Verde</option>
         <option value="amarelo">Amarelo</option>
       </select>
       <button onClick={handleAdd}>Enviar</button>
       
       
       </>)
}

export default Adicionar