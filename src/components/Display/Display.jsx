const Display = ({pessoas}) =>{

    return(<div className="times">
    
    <div>{pessoas.map(pessoa => pessoa.time === 'azul' ? <div className="azul">{pessoa.name}</div> : null)}</div>
    <div>{pessoas.map(pessoa =>pessoa.time === 'vermelho' ? <div className="vermelho">{pessoa.name}</div> : null)}</div>
    <div>{pessoas.map(pessoa => pessoa.time === 'verde' ? <div className="verde">{pessoa.name}</div> : null)}</div>
    <div>{pessoas.map(pessoa =>pessoa.time === 'amarelo' ? <div className="amarelo">{pessoa.name}</div> : null)}</div>
    
    </div>)
    
}

export default Display